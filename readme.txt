*** README File ***

DotControl Front-end Developer Assignment
By: David Dovale

Assignment: https://zpl.io/and0p4k
Text editor / IDE: Notepad++
Libraries, languages, formats etc: Bootstrap, HTML, CSS, PHP, JSON, jQuery, Javascript, 
Google Fonts, Google Icons, OMDB API, BitBucket, GIT

*** Disclaimer ****

Due to the lack of design information in the Zeplin dashboard, I selected a Google Font that was similar to the one 
shown in the design. I also used Chrome's color picker to get color codes, used estimates on spacing and took the 
DotControl logo from the official DotControl site. Under "normal" circumstances I would have obtained all of this information 
from the designers, but I chose this route to save time.

That led to some quick workarounds, such as adding more dark gradients to the top to cover the dark background on the logo. 
In a real assignment, I would have requested a transparent logo. Since I could also not see how the pop-up video was supposed to look,
I chose to go with a variant that loads an embedded Youtube video via iframe on a click of the poster. 

I loaded the OMDB info in a json file (along with the Youtube video info) and used PHP to show the results of the various movies. The idea 
is to show 6 more movies when clicking on the "load more" button, but I am not sure I will finish that within the alloted time frame.

*** Design feedback ***

I like this design a lot, but for a website that shows movie previews and information, I also expect filters and or sorting options.
It would be handy for a user to sort on rating, genre and recently added titles. A search option would also be handy, to quickly find 
a desired preview. I would also expect a bit more consistency between the leading movie title, and the smaller posters. 
The main title could also have a rating, or show the genre, while the smaller previews are missing the IMDb link.





