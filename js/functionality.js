(function($websiteMainRoot) {
  function initJumbotron() {
    $(window).load(function() {
      var jumbotronHeading = $websiteMainRoot.find(".jumbotronParent").find(".headingContainer"),
          imageContainer = $websiteMainRoot.find(".backgroundBannerParent"),
          contentUnits = $websiteMainRoot.find(".mainContentBody").find(".contentSegment");

      var tempImage = new Image();
      tempImage.setAttribute("src", "./assets/images/fixed-banner-bg.jpg");
      tempImage.addEventListener("load", function() {
        var $this = this,
            $thisSrc = $this.getAttribute("src");
        imageContainer.css({
          "background-image": "url(" + $thisSrc + ")"
        });
        jumbotronHeading.slideDown(2500);
        imageContainer.removeClass("hiddenTransform");
        contentUnits.removeClass("hiddenTransform");
      });
    });
  }

  function websiteCentralController() {
    initJumbotron();
  }

  $(document).ready(function() {
    websiteCentralController();
  });

})($("#outerContainer"))