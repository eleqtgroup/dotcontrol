<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/videoframe-styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mukta:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

   <title>Dot Control Front End Assignment</title>
  </head>
  <body>
  <div id="welcome">
  <div style="height:750px;width:100%;background-image:radial-gradient(transparent, #000000cc, black),linear-gradient(transparent, #000000);">
 <header>
 <div class="container pt25">
 <nav class="navbar navbar-expand-lg navbar-light">
   <a class="navbar-brand" href="#">
          <img src="img/logo.png" alt="" >
   </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>
</div>
 </header>
 <?php
$url = 'json/movies.json';
$data = file_get_contents($url); 
$movies = json_decode($data); // decode the JSON feed
?>
 <div class="container intro">
 <div class="col-md-7">
 <h1><?php echo $movies[0]->Title;?></h1>
 <p class="intro-desc"><?php echo $movies[0]->Plot;?></p>
 <a href="<?php echo $movies[0]->Link;?>" class="btn button-red round" target="_blank">More information <div class="imdb">IMDb</div></a>
 </div>
 </div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-md-4 mb30">
<div class="poster positionRelative videoFrame" id="playVideo1" style="background-image:url(<?php echo $movies[1]->Poster;?>)";>
<div class="poster-overlay">
<div class="info">
<h2><?php echo $movies[1]->Title;?></h2>
<h3><?php echo $movies[1]->Genre;?></h3>
<p class="score"><?php echo $movies[1]->imdbRating;?>/10</p>
</div>
</div>
</div>
</div>
<div class="videoBanner hiddenTransform" data-position="#playVideo1">
  <img src="img/close-icon.svg" class="img-responsive closeIcon" data-closer="#playVideo1"/>
	<iframe data-selection="#playVideo1" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $movies[1]->Youtube;?>?showinfo=0&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
</div>
<div class="col-md-4 mb30">
<div class="poster positionRelative videoFrame" id="playVideo2" style="background-image:url(<?php echo $movies[2]->Poster;?>)";>
<div class="poster-overlay">
<div class="info">
<h2><?php echo $movies[2]->Title;?></h2>
<h3><?php echo $movies[2]->Genre;?></h3>
<p class="score"><?php echo $movies[2]->imdbRating;?>/10</p>
</div>
</div>
</div>
</div>
<div class="videoBanner hiddenTransform" data-position="#playVideo2">
  <img src="img/close-icon.svg" class="img-responsive closeIcon" data-closer="#playVideo2"/>
	<iframe data-selection="#playVideo2" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $movies[2]->Youtube;?>?showinfo=0&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
</div>
<div class="col-md-4 mb30">
<div class="poster positionRelative videoFrame" id="playVideo3" style="background-image:url(<?php echo $movies[3]->Poster;?>)";>
<div class="poster-overlay">
<div class="info">
<h2><?php echo $movies[3]->Title;?></h2>
<h3><?php echo $movies[3]->Genre;?></h3>
<p class="score"><?php echo $movies[3]->imdbRating;?>/10</p>
</div>
</div>
</div>
</div>
<div class="videoBanner hiddenTransform" data-position="#playVideo3">
  <img src="img/close-icon.svg" class="img-responsive closeIcon" data-closer="#playVideo3"/>
	<iframe data-selection="#playVideo3" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $movies[3]->Youtube;?>?showinfo=0&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-md-4 mb30">
<div class="poster positionRelative videoFrame" id="playVideo4" style="background-image:url(<?php echo $movies[4]->Poster;?>)";>
<div class="poster-overlay">
<div class="info">
<h2><?php echo $movies[4]->Title;?></h2>
<h3><?php echo $movies[4]->Genre;?></h3>
<p class="score"><?php echo $movies[4]->imdbRating;?>/10</p>
</div>
</div>
</div>
</div>
<div class="videoBanner hiddenTransform" data-position="#playVideo4">
  <img src="img/close-icon.svg" class="img-responsive closeIcon" data-closer="#playVideo4"/>
	<iframe data-selection="#playVideo4" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $movies[4]->Youtube;?>?showinfo=0&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
</div>
<div class="col-md-4 mb30">
<div class="poster positionRelative videoFrame" id="playVideo5" style="background-image:url(<?php echo $movies[5]->Poster;?>)";>
<div class="poster-overlay">
<div class="info">
<h2><?php echo $movies[5]->Title;?></h2>
<h3><?php echo $movies[5]->Genre;?></h3>
<p class="score"><?php echo $movies[5]->imdbRating;?>/10</p>
</div>
</div>
</div>
</div>
<div class="videoBanner hiddenTransform" data-position="#playVideo5">
  <img src="img/close-icon.svg" class="img-responsive closeIcon" data-closer="#playVideo5"/>
	<iframe data-selection="#playVideo5" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $movies[5]->Youtube;?>?showinfo=0&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
</div>
<div class="col-md-4 mb30">
<div class="poster positionRelative videoFrame" id="playVideo6" style="background-image:url(<?php echo $movies[6]->Poster;?>)";>
<div class="poster-overlay">
<div class="info">
<h2><?php echo $movies[6]->Title;?></h2>
<h3><?php echo $movies[6]->Genre;?></h3>
<p class="score"><?php echo $movies[6]->imdbRating;?>/10</p>
</div>
</div>
</div>
</div>
<div class="videoBanner hiddenTransform" data-position="#playVideo6">
  <img src="img/close-icon.svg" class="img-responsive closeIcon" data-closer="#playVideo6"/>
	<iframe data-selection="#playVideo6" width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $movies[6]->Youtube;?>?showinfo=0&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
</div>
</div>
</div>
<div class="container text-center mb100">
 <a href="#" class="btn button-grey round"><i class="material-icons" style="transform:scaleX(-1) rotate(90deg);position:relative;top:5px;margin-right: 30px;">replay</i> Load more</a>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/main.js"></script>
<script src="js/functionality.js"></script>
</body>